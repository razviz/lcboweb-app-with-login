package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginValidator {

	public static boolean isValidLoginName(String loginName) {
		String regex = "^[A-Za-z]\\w{5,29}$";
		Pattern pattern = Pattern.compile(regex);
		Matcher match = pattern.matcher(loginName);

		return loginName.length() >= 6 && match.matches();
	}
	
	public static void main(String args[]) {
		
		//empty main method
	}
}
