package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	//Tests that it has 6 characters
	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "zainrazvi" ) );
	}
	
	@Test
	public void testIsValidLoginException() {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "zai" ) );
	}

	@Test
	public void testIsValidLoginBoundaryIn() {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "zainra" ) );
	}
	
	@Test
	public void testIsValidLoginBoundaryOut() {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "zainr" ) );
	}
	
	//Tests that it starts with a letter
	@Test
	public void testIsValidLoginStartsWithLetterRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "razvizain123" ) );
	}
	
	@Test
	public void testIsValidLoginStartsWithLetterException() {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "1za" ) );
	}

	@Test
	public void testIsValidLoginStartsWithLetterBoundaryIn() {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "zainer1" ) );
	}
	
	@Test
	public void testIsValidLoginStartsWithLetterBoundaryOut() {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "1zainra" ) );
	}
	
}
